import React, { Component } from 'react';

import { Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';

// Styles
import './styles/main.css'
import { MuiThemeProvider } from 'material-ui/styles';
import mainTheme from './themes/mainTheme';
import 'typeface-roboto';

// Views

import Login from './views/Login';
import Home from './views/Home';
import Base from './views/Base';
import NotFound from './views/NotFound';

// Firebase

import fbConfig from './db/firebase-config';
import firebase from 'firebase';

firebase.initializeApp(fbConfig);

function PrivateRoute ({component: Component, authed, ...rest}) {
  return (
    <Route
      {...rest}
      render={(props) => authed === true
        ? <Component {...props} />
        : <Redirect to={{pathname: '/login', state: {from: props.location}}} />}
    />
  )
}

function PublicRoute ({component: Component, authed, ...rest}) {
  return (
    <Route
      {...rest}
      render={(props) => authed === false
        ? <Component {...props} />
        : <Redirect to='/home' />}
    />
  )
}

class App extends Component {
  state = {
    authed: false,
    loading: true
  }

  componentDidMount () {
    this.removeListener = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({
          authed: true,
          loading: false,
        })
      } else {
        this.setState({
          authed: false,
          loading: false
        })
      }
    })
  }

  componentWillUnmount () {
    this.removeListener()
  }

  render() {
    return this.state.loading === true ? <h1>Loading</h1> : (
      <BrowserRouter>
        <MuiThemeProvider theme={mainTheme}>
          <Switch>
            <PrivateRoute authed={this.state.authed} path="/home" component={Home} />
            <PublicRoute authed={this.state.authed} path='/login' component={Login} />
            <Route exact path="/" component={Base} />
            <Route component={NotFound} />
          </Switch>
        </MuiThemeProvider>
      </BrowserRouter>
    );
  }
}

export default App;
