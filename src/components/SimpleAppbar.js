import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';

const styles = theme => ({
  root: {
    width: '100%',
    textAlign: 'center',
  },
});	

class SimpleAppbar extends Component {
  constructor(props) {
    super(props);
    this.classes = props.classes;
  }

  render() {
    return (
      <div>
	  	<AppBar position="static" className={this.classes.root}>
	    	<Toolbar>
	          <Typography type="title" color="inherit" align="center">
	            Rewind
	          </Typography>
	        </Toolbar>
	  	</AppBar>
	  </div>
    );
  }
}

export default withStyles(styles)(SimpleAppbar);
